import React from 'react';
import { Carousel } from 'antd';

const PortfolioCarousel = () => {
  return (
    <Carousel arrows={true} slidesPerRow={3} infinite={true} dots={false}>
      <div className="test">
        <p>1</p>
      </div>
      <div className="test">
        <p>2</p>
      </div>
      <div className="test">
        <p>3</p>
      </div>
      <div className="test">
        <p>4</p>
      </div>
      <div className="test">
        <p>6</p>
      </div>
    </Carousel>
  );
};

export default PortfolioCarousel;

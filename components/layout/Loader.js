import React from 'react';
import { Typography, Spin } from "antd"

const Loader = ({ children, loading }) => {
  return <Spin spinning={loading} size='large'>{children}</Spin>;
};

export default Loader;

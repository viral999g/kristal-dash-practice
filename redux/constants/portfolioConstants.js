export const GET_PORTFOLIOS = 'GET_PORTFOLIOS'

export const GET_PORTFOLIOS_REQUEST = 'GET_PORTFOLIOS_REQUEST'
export const GET_PORTFOLIOS_SUCCESS = 'GET_PORTFOLIOS_SUCCESS'
export const GET_PORTFOLIOS_FAIL = 'GET_PORTFOLIOS_FAIL'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import watcherSaga from './sagas/rootSaga';
import { portfoliosReducer } from './reducers/portfolioReducers';

const reducer = combineReducers({
  portfolios: portfoliosReducer
})

const sagaMiddleware = createSagaMiddleware()

const middleware = [sagaMiddleware]

const store = createStore(reducer, {}, composeWithDevTools(applyMiddleware(...middleware)))

sagaMiddleware.run(watcherSaga)

export default store
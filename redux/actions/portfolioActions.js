import { GET_PORTFOLIOS, GET_PORTFOLIOS_FAIL, GET_PORTFOLIOS_REQUEST, GET_PORTFOLIOS_SUCCESS } from "../constants/portfolioConstants"

export const getPortfolios = () => ({
  type: GET_PORTFOLIOS
})

export const getPortfoliosLoading = () => ({
  type: GET_PORTFOLIOS_REQUEST,
})

export const getPortfoliosSuccess = (portfolios) => ({
  type: GET_PORTFOLIOS_SUCCESS,
  payload: portfolios
})

export const getPortfoliosFail = (error) => ({
  type: GET_PORTFOLIOS_FAIL,
  payload: error
})
import axios from "axios";

export function requestGetPortfolios() {
  return axios.request({
    method: "get",
    url: "https://dev.kristal.ai/kristals-ws/api/v3/getLandingPage",
    headers: {
      "Access-Token": "NzJjYzhkNTgtZTZhNC00ZmMyLTk0MmUtMzNjNTY2YmU0OGE5JVVTRVIlNzg=",
      "User-ID": "78",
      "PLATFORM_CODE": "WEBSITE",
      "Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36",
    }
  });
}


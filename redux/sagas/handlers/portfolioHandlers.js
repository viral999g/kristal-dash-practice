import { call, put } from "redux-saga/effects";
import { getPortfoliosFail, getPortfoliosLoading, getPortfoliosSuccess } from "../../actions/portfolioActions";
import { requestGetPortfolios } from "../requests/porfolioRequests";

export function* handleGetPortfolios(action) {
  try {
    yield put(getPortfoliosLoading());
    const response = yield call(requestGetPortfolios);
    const { data } = response;
    yield put(getPortfoliosSuccess(data.elementList));
  } catch (error) {
    yield put(getPortfoliosFail('Something went wrong'))
  }
}
import { all, fork } from 'redux-saga/effects'
import * as portfolioSagas from './portfolioSaga'

export default function* watcherSaga() {
  yield all(
    [...Object.values(portfolioSagas)].map(fork)
  );
}
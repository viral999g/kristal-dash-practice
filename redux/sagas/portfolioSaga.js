import { takeLatest } from "redux-saga/effects";
import { getPortfolios } from "../actions/portfolioActions";
import { handleGetPortfolios } from "./handlers/portfolioHandlers";

export function* watchGetPortfolios() {
  yield takeLatest(getPortfolios().type, handleGetPortfolios)
}
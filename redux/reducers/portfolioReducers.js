import { GET_PORTFOLIOS_FAIL, GET_PORTFOLIOS_REQUEST, GET_PORTFOLIOS_SUCCESS } from "../constants/portfolioConstants";

const initialState = {
  loading: false,
  portfolios: [],
  error: ''
}

export const portfoliosReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PORTFOLIOS_REQUEST:
      return { ...state, loading: true };
    case GET_PORTFOLIOS_SUCCESS:
      return { loading: false, portfolios: action.payload, error: '' };
    case GET_PORTFOLIOS_FAIL:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
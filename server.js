const express = require('express')
const next = require('next')
const dotenv = require('dotenv')

dotenv.config();

const PORT = process.env.PORT || 3000
const dev = process.env.NODE_ENV !== 'production'

const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()
  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(PORT, () => {
    console.log(`Server listening at port ${PORT}`)
  })
})
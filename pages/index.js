import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Typography, Skeleton } from "antd"
import { getPortfolios } from '../redux/actions/portfolioActions'
import Loader from '../components/layout/Loader'
import PortfolioCarousel from '../components/portfoliosSection/PortfolioCarousel'

const { Title } = Typography

export default function Home() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getPortfolios())
  }, [dispatch])

  const { loading, portfolios } = useSelector(state => state.portfolios)

  return (
    <Loader loading={loading}>
      {/* {portfolios.map(element => (
        <PortfolioCarousel level={2} key={element.searchFilterName}></PortfolioCarousel>
      ))} */}
      <PortfolioCarousel></PortfolioCarousel>
    </Loader>
  )
}

import '../styles/less/global.less'
import '../styles/less/antd.less'
import '../styles/sass/style.scss'

import { Provider } from 'react-redux'
import store from '../redux/store'

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default MyApp
